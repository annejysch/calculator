let input = document.getElementById('inputBox'); //element of declaration in input the index
let buttons = document.querySelectorAll('button'); //option selection of the different buttons

let string = "";
let arr = Array.from(buttons);

arr.forEach(button => {
    
    button.addEventListener('click', (e)=>{ //action with click
        if (e.target.innerHTML == '=') { //comparison with equal button
            string = val(string);
            input.value = string;
        } else if (e.target.innerHTML == 'AC') { //clear screen
            string = "";
            input.value = string;
        } else if(e.target.innerHTML == 'DEL'){ //delete of character
            string = string.substring(0, string.length-1);
            input.value = string;
        } else {
            //funcion for the buttons in the screen of calculator, responds to click
            string += e.target.innerHTML;
            input.value = string;
        }


    })
})